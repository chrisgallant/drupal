<?php

  function spring_preprocess_page(&$variables){
    $page = $variables['page'];
    $sidebar_class = 'one-fourth';
    $content_class = 'three-fourths';
    $variables['page']['tpl_control']=[];

    if(empty($page['sidebar_left'])){
      $sidebar_class = '';
      $content_class = 'full-width';
    }

    $variables['page']['tpl_control']['sidebar_class'] = $sidebar_class;
    $variables['page']['tpl_control']['content_class'] = $content_class;

  }
